import { configureStore } from "@reduxjs/toolkit";
import usersReducer from "../usersReducer";
import subscriptionReducer from "../subscribtionReducer";
import visibilityReducer from "../visibilityReducer";

const store = configureStore({
  reducer: {
    user: usersReducer,
    subscription: subscriptionReducer,
    visibility: visibilityReducer,
  },
});

export default store;
