import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import "./index.css";
const UserDetailsPage = () => {
  const { id } = useParams();
  const [userData, setUserData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios
      .get(`/community/${id}`)
      .then((response) => {
        setUserData(response.data);
        setLoading(false);
        console.log("Fetched User Data:", response.data);
      })
      .catch((error) => {
        setError(error.message || "Error fetching user data");
        setLoading(false);
        console.error("Error fetching user data:", error);
      });
  }, [id]);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  if (!userData) {
    return <div>User not found</div>;
  }

  return (
    <div className="card">
      <h2>User Details</h2>
      <img
        className="testimonial-img"
        src={userData.avatar}
        alt={`${userData.firstName} ${userData.lastName}`}
      />
      <p className="testimonial-text">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolor.
      </p>
      <div className="testimonial-author">
        <div className="testimonial-author-firstname">{userData.firstName}</div>
        <div className="testimonial-author-lastname">{userData.lastName}</div>
      </div>
      <div className="testimonial-position">{userData.position}</div>
    </div>
  );
};

export default UserDetailsPage;
