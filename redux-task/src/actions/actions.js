export const TOGGLE_SUBSCRIPTION = "TOGGLE_SUBSCRIPTION";
export const TOGGLE_SECTION_VISIBILITY = "TOGGLE_SECTION_VISIBILITY";
export const toggleSubscription = () => {
  return function (dispatch) {
    dispatch({
      type: TOGGLE_SUBSCRIPTION,
    });
  };
};

export const toggleSectionVisibility = () => {
  return function (dispatch) {
    dispatch({
      type: TOGGLE_SECTION_VISIBILITY,
    });
  };
};
