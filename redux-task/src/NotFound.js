import React from "react";
import { Link } from "react-router-dom";
import "./index.css";
function NotFound() {
  return (
    <div className="not-found">
      <h1>Page not found</h1>
      <p>
        Looks like you have followed a broken link or entered a URl that doesn't
        exist on this site
      </p>
      <Link to="/" className="link-style">
        &larr; Back to our site
      </Link>
    </div>
  );
}

export default NotFound;
